/*
Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.
*/

// Using promises and the `fetch` library, do the following. 
// If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.
// Usage of the path libary is recommended

// 1. Fetch all the users
// 2. Fetch all the todos
// 3. Use the promise chain and fetch the users first and then the todos.
// 4. Use the promise chain and fetch the users first and then all the details for each user.
// 5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with

const fs = require('fs');
const path = require('path');


// 1. Fetch all the users
function fetchUser(userApi) {
    fetch(userApi)
        .then(res => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error(`${response.status}`);
            }
        })
        .then((users) => {
            fs.writeFile(
                path.join(__dirname, "users.json"),
                JSON.stringify(users),
                (err) => {
                    if (err) {
                        throw new Error(err);
                    } else {
                        Promise.resolve();
                    }
                })
        })
        .then(() => {
            console.log("users saved into users.json");
        })
        .catch((err) => {
            console.error(err);
        })
}



// 2. Fetch all the todos
function fetchTodos(todosAPI) {
    fetch(todosAPI)
        .then((response) => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error(`${response.status}`);
            }
        })
        .then((todosData) => {
            fs.writeFile(
                path.join(__dirname, 'todos.json'),
                JSON.stringify(todosData),
                ((err) => {
                    if (err) {
                        throw new Error(err);
                    } else {
                        Promise.resolve();
                    }
                })
            )
        })
        .then(() => {
            console.log("todos file saved..");
        })
        .catch((err) => {
            console.error("error: ", err);
        })
}
// fetchTodos('https://jsonplaceholder.typicode.com/todos');



